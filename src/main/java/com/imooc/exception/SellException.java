package com.imooc.exception;

import com.imooc.enums.ResultEnum;
import lombok.Data;

/**
 * Created by xzf on 2017/10/18.
 */
@Data
public class SellException extends RuntimeException {

    private Integer code;

    public SellException(ResultEnum resultEnum) {

        super(resultEnum.getMsg());
        this.code=resultEnum.getCode();
    }

    public SellException(Integer code,String msg) {
        super(msg);
        this.code = code;
    }
}
