package com.imooc.dto;

import lombok.Data;

/**
 * 购物车
 * Created by xzf on 2017/10/18.
 */
@Data
public class CartDTO {

    //商品
    private String productId;
    //数量
    private Integer productQuantity;

    public CartDTO(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
