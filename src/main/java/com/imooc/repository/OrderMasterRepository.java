package com.imooc.repository;

import com.imooc.dataobject.OrderMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by xzf on 2017/10/17.
 */
public interface OrderMasterRepository extends JpaRepository<OrderMaster,String>{

    /**不传Pageable，则查出某个人所有订单*/
    Page<OrderMaster> findByBuyerOpenid(String buyerOpenid, Pageable pageable);
}
