package com.imooc.enums;

import lombok.Getter;

/**
 * Created by xzf on 2017/10/17.
 */
@Getter
public enum PayStatusEnum {

    WAIT(0,"未支付"),
    SUCCESS(1,"支付成功"),

    ;

    private Integer code;
    private String msg;

    PayStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
