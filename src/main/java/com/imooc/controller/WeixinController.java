package com.imooc.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 此处是手工获取openid的方法，不推荐，建议使用第三方SDK简化开发
 * Created by xzf on 2017/11/2.
 */
@RestController
@Slf4j
@RequestMapping("/weixin")
public class WeixinController {

    @GetMapping("/auth")
//  微信访问回调域名地址时进入到该方法
    public String auth(@RequestParam("code") String code){
        log.info("进入auth方法");
        log.info("code={}",code);

        //使用RestTemplate发起Http请求
        String url="https://api.weixin.qq.com/sns/oauth2/access_token?" +
                "appid=wx6d6d0eabe8392e5a" +
                "&secret=6be3dff05e5da4584abdc9e90146b36b" +
                "&code=" +code+
                "&grant_type=authorization_code";
        RestTemplate restTemplate=new RestTemplate();
        String response=restTemplate.getForObject(url,String.class);//get请求
        log.info("response={}",response);
        return response;

    }
}
