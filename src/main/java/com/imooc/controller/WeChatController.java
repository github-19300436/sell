package com.imooc.controller;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xzf on 2017/11/2.
 */
@RestController
@RequestMapping("/wechat")
public class WeChatController {

    @Autowired
    private WxMpService wxMpService;


    @GetMapping("/authorize")
    public void authorize(@RequestParam("returnUrl") String returnUrl){
        //1.配置
        //2.调用方法
        String url="";
        wxMpService.oauth2buildAuthorizationUrl(url, WxConsts.OAUTH2_SCOPE_USER_INFO,"");

    }
}
